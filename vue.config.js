const fs = require('fs');
let https = false;
if (fs.existsSync('./certs/localhost.key') && fs.existsSync('./certs/localhost.crt')) {
  https = {
    key: fs.readFileSync('./certs/localhost.key'),
    cert: fs.readFileSync('./certs/localhost.crt')
  }
}
// vue.config.js
module.exports = {
  "devServer": {
    "port": 6042,
    // "https": https
  },
  configureWebpack: config => {
    if (process.env.NODE_ENV === 'production') {
      config.resolve.alias.vue$ = 'vue/dist/vue.min.js'
    } else {
      config.resolve.alias.vue$ = 'vue/dist/vue.js'
    }
  }
};