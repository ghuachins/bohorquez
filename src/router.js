import Vue from 'vue'
import Router from 'vue-router'

import Home from './views/home/Home'
import About from './views/about/About'
import Products from './views/products/Products'
import ProductDetail from './views/product-detail/ProductDetail'
import Pisco from './views/pisco/Pisco'
import Contact from './views/contact/Contact'

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
      meta: {
        layout: 'carousel'
      }
    },
    {
      path: '/bohorquez',
      name: 'about',
      component: About
    },
    {
      path: '/productos',
      name: 'products',
      component: Products
    },
    {
      path: '/productos/:name_slug-:id',
      name: 'product-detail',
      component: ProductDetail
    },
    {
      path: '/el-pisco',
      name: 'pisco',
      component: Pisco
    },
    {
      path: '/contactanos',
      name: 'contact',
      component: Contact
    },
  ]
})
